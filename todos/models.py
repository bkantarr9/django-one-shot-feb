from django.db import models

# Create your models here.
# Feature 3
class TodoList(models.Model):
    name = models.CharField("Name", max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name
    


# Feature 5 
class TodoItem(models.Model):
    task = models.CharField("task", max_length=100)
    due_date = models.DateTimeField("due date", null=True, blank=True)
    is_completed = models.BooleanField("is_complicated", default=False)
    list = models.ForeignKey(
        "TodoList",
        related_name="items",
        on_delete=models.CASCADE,
    )