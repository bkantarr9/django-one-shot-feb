from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
# Feature 4

@admin.register(TodoList)
class TodoList(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

# Feature 6

@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
        "id",
    ]

